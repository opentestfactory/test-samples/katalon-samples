<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>4_TestSuite_Iteration_Data</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8aafe634-a04e-4191-aadb-f47f3267ced5</testSuiteGuid>
   <testCaseLink>
      <guid>507d581a-89e8-486b-9cce-54b4706010e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4_Iteration_Data/4_KAID_TestCase_01_With_LocalID</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ea162565-b475-413c-8f39-d9a8d7dc954b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Iteration_Data/IdsCsvFile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ea162565-b475-413c-8f39-d9a8d7dc954b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ids</value>
         <variableId>7721233a-4125-4735-9fd5-bb94e5b44d07</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
